package com.wrike.webtests.blocks.mainpage;

import org.openqa.selenium.Rectangle;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class FolderViewBlock extends HtmlElement {

    @Name("Список проектов")
    @FindBy(css = ".wspace-navigation-project")
    private List<HtmlElement> folders;

    @Name("Поле содания нового проекта")
    @FindBy(css = "[style=''] input")
    private HtmlElement newProject;

    public List<HtmlElement> folders() {
        return folders;
    }

    public HtmlElement newProject() {
        return newProject;
    }

    @Override
    public Rectangle getRect() {
        return null;
    }
}
