package com.wrike.webtests.blocks.mainpage;

import org.openqa.selenium.Rectangle;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class NavigationPanel extends HtmlElement {

    @Name("Вкладка «Проекты»")
    @FindBy(id = "folder-tab")
    private FolderTabBlock folderTab;

    @Name("Навигация по проектам")
    @FindBy(id = "folderTreeView")
    private FolderViewBlock folderView;

    public FolderTabBlock folderTab() {
        return folderTab;
    }

    public FolderViewBlock folderView() {
        return folderView;
    }

    @Override
    public Rectangle getRect() {
        return null;
    }

}
