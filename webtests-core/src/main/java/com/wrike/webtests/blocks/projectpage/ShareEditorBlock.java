package com.wrike.webtests.blocks.projectpage;

import org.openqa.selenium.Rectangle;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class ShareEditorBlock extends HtmlElement {

    @Name("Плашка «Нет контактов»")
    @FindBy(css = "p")
    private HtmlElement notContactsToShare;

    @Name("Список юзеров для расшаривания")
    @FindBy(css = ".user-item>.user-item-wrap")
    private List<HtmlElement> usersToShareList;

    @Name("Список с доступами")
    @FindBy(css = ".wrike-contact-user-list-view .user-list")
    private List<AcceptedUserBlock> usersAcceptedList;

    public HtmlElement notContactsToShare() {
        return notContactsToShare;
    }

    public List<HtmlElement> usersToShareList() {
        return usersToShareList;
    }

    public List<AcceptedUserBlock> usersAcceptedList() {
        return usersAcceptedList;
    }

    @Override
    public Rectangle getRect() {
        return null;
    }

}
