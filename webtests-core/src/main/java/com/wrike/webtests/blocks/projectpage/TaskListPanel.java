package com.wrike.webtests.blocks.projectpage;

import org.openqa.selenium.Rectangle;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class TaskListPanel extends HtmlElement {

    @Name("Кнопка «Новая задача»")
    @FindBy(css = "[wrike-task-view-adder-big]")
    private HtmlElement addTaskButton;

    @Name("Поле ввода новой задачи")
    @FindBy(css = "input[wrike-task-view-title-editor-input]")
    private HtmlElement newTaskInput;

    @Name("Список тасков")
    @FindBy(css = "a[wrike-task-view-row-info-plate][href]")
    private List<HtmlElement> tasksList;

    public List<HtmlElement> tasksList() {
        return tasksList;
    }

    public HtmlElement newTaskInput() {
        return newTaskInput;
    }

    public HtmlElement addTaskButton() {
        return addTaskButton;
    }

    @Override
    public Rectangle getRect() {
        return null;
    }

}
