package com.wrike.webtests.blocks.loginpage;

import org.openqa.selenium.Rectangle;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class LoginBlock extends HtmlElement {

    @Name("Поле ввода логина")
    @FindBy(id = "emailField")
    private HtmlElement emailField;

    @Name("Поле ввода пароля")
    @FindBy(id = "passwordField")
    private HtmlElement passwordField;

    @Name("Кнопка «Войти»")
    @FindBy(id = "submit-login-button")
    private HtmlElement loginButton;

    public HtmlElement email() {
        return emailField;
    }

    public HtmlElement password() {
        return passwordField;
    }

    public HtmlElement loginButton() {
        return loginButton;
    }

    @Override
    public Rectangle getRect() {
        return null;
    }
}
