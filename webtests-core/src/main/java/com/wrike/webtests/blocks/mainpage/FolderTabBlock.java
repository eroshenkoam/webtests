package com.wrike.webtests.blocks.mainpage;

import org.openqa.selenium.Rectangle;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class FolderTabBlock extends HtmlElement {

    @Name("Кнопка поиска")
    @FindBy(css = ".wspace-tree-searchButton")
    private HtmlElement searchButton;

    @Name("Добавить проект")
    @FindBy(css = "use")
    private HtmlElement addProjectButton;

    public HtmlElement searchButton() {
        return searchButton;
    }

    public HtmlElement addProjectButton() {
        return addProjectButton;
    }

    @Override
    public Rectangle getRect() {
        return null;
    }
}
