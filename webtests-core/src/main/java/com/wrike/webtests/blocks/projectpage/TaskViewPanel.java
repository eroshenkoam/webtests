package com.wrike.webtests.blocks.projectpage;

import org.openqa.selenium.Rectangle;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class TaskViewPanel extends HtmlElement {

    @Name("Название таска")
    @FindBy(css = ".wrike-panel-header-info textarea")
    private HtmlElement taskName;

    @Name("Кнопка «Расшарить задачу»")
    @FindBy(css = ".wspace-task-widgets-share-button")
    private HtmlElement shareButton;

    public HtmlElement taskName() {
        return taskName;
    }

    public HtmlElement shareButton() {
        return shareButton;
    }

    @Override
    public Rectangle getRect() {
        return null;
    }

}
