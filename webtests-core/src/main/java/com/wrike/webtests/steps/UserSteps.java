package com.wrike.webtests.steps;

import com.google.inject.Inject;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class UserSteps {

    private WebDriverSteps webDriverSteps;

    private ProjectSteps projectSteps;

    private LoginSteps loginSteps;

    @Inject
    public UserSteps(WebDriverSteps webDriverSteps, ProjectSteps projectSteps, LoginSteps loginSteps) {
        this.webDriverSteps = webDriverSteps;
        this.projectSteps = projectSteps;
        this.loginSteps = loginSteps;
    }

    public WebDriverSteps defaultSteps() {
        return webDriverSteps;
    }

    public ProjectSteps projectSteps() {
        return projectSteps;
    }

    public LoginSteps loginSteps() {
        return loginSteps;
    }
}
