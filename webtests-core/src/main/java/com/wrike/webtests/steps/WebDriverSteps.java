package com.wrike.webtests.steps;

import com.google.inject.Inject;
import com.wrike.webtests.webdriver.WebDriverManager;
import org.hamcrest.Matcher;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import static com.wrike.webtests.Utils.withWaitFor;
import static com.wrike.webtests.TestConfig.config;

import static javax.ws.rs.core.UriBuilder.fromUri;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.AllOf.allOf;
import static ru.yandex.qatools.htmlelements.matchers.WebElementMatchers.isDisplayed;
import static ru.yandex.qatools.htmlelements.matchers.WrapsElementMatchers.exists;
import static ru.yandex.qatools.htmlelements.matchers.WrapsElementMatchers.hasText;
import static ru.yandex.qatools.htmlelements.matchers.WrapsElementMatchers.hasValue;

import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.hasSize;
import static org.openqa.selenium.Keys.ENTER;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class WebDriverSteps {

    private WebDriverManager driverManager;

    @Inject
    public WebDriverSteps (WebDriverManager driverManager) {
        this.driverManager = driverManager;
    }

    @Step("Открываем страницу «{0}»")
    public void openUrl(String url) {
        getDriver().get(url);
    }

    @Step("Открываем страницу по пути «{0}»")
    public WebDriverSteps openPath(String path) {
        openUrl(fromUri(config().betaUri()).path(path).build().toString());
        return this;
    }

    @Step("Кликаем по «{0}»")
    public WebDriverSteps clickOn(HtmlElement element) {
        element.click();
        return this;
    }

    @Step("Клик по элементу который содержит текст «{1}» из списка «{0}»")
    public WebDriverSteps clicksOnElementWithText(List<HtmlElement> elements, String text) {
        HtmlElement elementWithText = elements.stream().filter(element ->
                hasText(containsString(text)).matches(element)).findFirst().get();
        clickOn(elementWithText);
        return this;
    }

    @Step("Должны видеть «{0}»")
    public WebDriverSteps shouldSee(HtmlElement element) {
        assertThat("Должны видеть элемент", element, withWaitFor(allOf(exists(), isDisplayed())));
        return this;
    }

    @Step("Должны видеть текст «{1}» у элемента «{0}»")
    public WebDriverSteps shouldSeeText(HtmlElement element, Matcher<String> matcher) {
        assertThat("Должны видеть текст элемента", element, withWaitFor(hasText(matcher)));
        return this;
    }

    @Step("Должны видеть значение «{1}» у элемента «{0}»")
    public WebDriverSteps shouldSeeValue(HtmlElement element, Matcher<String> matcher) {
        assertThat("Должны видеть текст элемента", element, withWaitFor(hasValue(matcher)));
        return this;
    }

    @Step("Список «{0}» должен содержать «{1}» элемент/а/ов")
    public WebDriverSteps shouldSeeListCount(List<? extends HtmlElement> elements, int size) {
        assertThat("Должны видеть нужное количество элементов", elements, hasSize(size));
        return this;
    }

    @Step("Вводим «{1}» в поле «{0}»")
    public WebDriverSteps inputsTextTo(HtmlElement element, String text) {
        element.sendKeys(text);
        return this;
    }

    @Step("Подтверждаем ввод в «{0}»")
    public WebDriverSteps pressEnter(HtmlElement element) {
        element.sendKeys(ENTER);
        return this;
    }

    @Step("Текущее окно")
    public String windowHandle() {
        return getDriver().getWindowHandle();
    }

    @Step("Открываем новое окно")
    public WebDriverSteps openNewWindow() {
        ((JavascriptExecutor)getDriver()).executeScript("window.open();");
        return this;
    }

    @Step("Закрываем окно")
    public WebDriverSteps closeCurrentWindow() {
        getDriver().close();
        return this;
    }

    @Step("Переходим в нужное окно")
    public WebDriverSteps switchTo(String window) {
        getDriver().switchTo().window(window);
        return this;
    }

    @Step("Открываем второе окно с текущим URL")
    public WebDriverSteps openNewWindowOnSameUrl() {
        String currentUrl = getDriver().getCurrentUrl();
        String handle = getDriver().getWindowHandle();

        openNewWindow();
        Set<String> set = getDriver().getWindowHandles();
        set.remove(handle);

        getDriver().switchTo().window((String)set.toArray()[0]);
        openUrl(currentUrl);
        return this;
    }

    private WebDriver getDriver() {
        return driverManager.get();
    }

}
