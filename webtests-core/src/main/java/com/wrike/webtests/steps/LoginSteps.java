package com.wrike.webtests.steps;

import com.google.inject.Inject;
import com.wrike.webtests.pages.LoginPage;
import com.wrike.webtests.webdriver.WebDriverManager;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class LoginSteps {

    private WebDriverManager driverManager;

    private WebDriverSteps webDriverSteps;

    @Inject
    public LoginSteps(WebDriverManager driverManager, WebDriverSteps webDriverSteps) {
        this.driverManager = driverManager;
        this.webDriverSteps = webDriverSteps;
    }

    @Step("Логинимся пользователем «{0} : {1}»")
    public LoginSteps login(String email, String password) {
        webDriverSteps.openPath("login")
                .shouldSee(onLoginPage().loginBlock())
                .inputsTextTo(onLoginPage().loginBlock().email(), email)
                .inputsTextTo(onLoginPage().loginBlock().password(), password)
                .clickOn(onLoginPage().loginBlock().loginButton());
        return this;
    }

    private LoginPage onLoginPage() {
        return new LoginPage(getDriver());
    }

    private WebDriver getDriver() {
        return driverManager.get();
    }
}
