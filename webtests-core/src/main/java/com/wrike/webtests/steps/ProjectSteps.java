package com.wrike.webtests.steps;

import com.google.inject.Inject;
import com.wrike.webtests.pages.MainPage;
import com.wrike.webtests.pages.ProjectPage;
import com.wrike.webtests.webdriver.WebDriverManager;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Step;

import static org.hamcrest.Matchers.equalTo;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class ProjectSteps {

    private WebDriverManager driverManager;

    private WebDriverSteps webDriverSteps;

    @Inject
    public ProjectSteps(WebDriverManager driverManager, WebDriverSteps webDriverSteps) {
        this.driverManager = driverManager;
        this.webDriverSteps = webDriverSteps;
    }

    @Step("Создаём новый таск с именем «{0}»")
    public ProjectSteps createNewTask(String taskName) {
        webDriverSteps.shouldSee(onProjectPage().taskListPanel().addTaskButton())
                .clickOn(onProjectPage().taskListPanel().addTaskButton())
                .shouldSee(onProjectPage().taskListPanel().newTaskInput())
                .inputsTextTo(onProjectPage().taskListPanel().newTaskInput(), taskName)
                .pressEnter(onProjectPage().taskListPanel().newTaskInput())
                .shouldSee(onProjectPage().taskListPanel())
                .shouldSee(onProjectPage().taskViewPanel())
                .shouldSeeListCount(onProjectPage().taskListPanel().tasksList(), 1)
                .clicksOnElementWithText(onProjectPage().taskListPanel().tasksList(), taskName)
                .shouldSeeValue(onProjectPage().taskViewPanel().taskName(), equalTo(taskName));
        return this;
    }

    @Step("Создаём новый проект с именем «{0}»")
    public ProjectSteps createNewProject(String projectName) {
        webDriverSteps
                .shouldSee(onMainPage().navigationPanel().folderTab().addProjectButton())
                .clickOn(onMainPage().navigationPanel().folderTab().addProjectButton())
                .shouldSee(onMainPage().navigationPanel().folderView().newProject())
                .clickOn(onMainPage().navigationPanel().folderView().newProject())
                .inputsTextTo(onMainPage().navigationPanel().folderView().newProject(), projectName)
                .pressEnter(onMainPage().navigationPanel().folderView().newProject())
                .shouldSee(onProjectPage().taskViewPanel())
                .shouldSee(onProjectPage().taskListPanel());
        return this;
    }

    private MainPage onMainPage() {
        return new MainPage(getDriver());
    }

    private ProjectPage onProjectPage() {
        return new ProjectPage(getDriver());
    }

    private WebDriver getDriver() {
        return driverManager.get();
    }
}

