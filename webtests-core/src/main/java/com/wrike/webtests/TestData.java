package com.wrike.webtests;

import com.wrike.webtests.beans.User;
import org.kohsuke.randname.RandomNameGenerator;

import static java.lang.String.format;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class TestData {

    public static RandomNameGenerator rnd =  new RandomNameGenerator();

    public static String getRandomName() {
        return rnd.next();
    }

    public static String getRandomTaskName() {
        return format("task_%s", rnd.next());
    }

    public static String getRandomProjectName() {
        return format("folder_%s", rnd.next());
    }

    public static User getAvailableUser() {
        return new User().withEmail("qweb.test1@yandex.ru").withPassword("testqa");
    }

}
