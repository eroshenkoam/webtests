package com.wrike.webtests;

import ru.qatools.properties.Property;
import ru.qatools.properties.PropertyLoader;
import ru.qatools.properties.providers.MapPropPathReplacerProvider;

import java.net.URI;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class TestConfig {
    private static TestConfig instance;

    public static TestConfig config() {
        if (null == instance) {
            instance = new TestConfig() ;
        }
        return instance;
    }

    private TestConfig() {
        PropertyLoader.newInstance()
                .withPropertyProvider(new MapPropPathReplacerProvider(System.getProperties()))
                .populate(this);
    }

    @Property("beta.host")
    private URI betaUri = URI.create("https://wrike.com");

    @Property("prod.host")
    private URI prodUri = URI.create("https://wrike.com");

    public URI betaUri() {return betaUri;}

    public URI prodUri() {return prodUri;}

}
