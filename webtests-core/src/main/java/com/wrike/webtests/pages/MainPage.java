package com.wrike.webtests.pages;

import com.wrike.webtests.blocks.mainpage.NavigationPanel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class MainPage {

    public MainPage(WebDriver driver) {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(driver)), this);
    }


    @Name("Колонка навигации")
    @FindBy(id = "global-navigation-panel")
    private NavigationPanel navigationPanel;

    public NavigationPanel navigationPanel() {
        return navigationPanel;
    }

}
