package com.wrike.webtests.pages;

import com.wrike.webtests.blocks.mainpage.NavigationPanel;
import com.wrike.webtests.blocks.projectpage.ShareEditorBlock;
import com.wrike.webtests.blocks.projectpage.TaskListPanel;
import com.wrike.webtests.blocks.projectpage.TaskViewPanel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class ProjectPage {

    public ProjectPage(WebDriver driver) {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(driver)), this);
    }

    @Name("Колонка навигации")
    @FindBy(id = "global-navigation-panel")
    private NavigationPanel navigationPanel;

    @Name("Панель с задачами")
    @FindBy(id = "tasklist-panel")
    private TaskListPanel taskListPanel;

    @Name("Обзор задачи")
    @FindBy(css = ".wspace-task-view")
    private TaskViewPanel taskViewPanel;

    @Name("wrike-shareeditor-small")
    @FindBy(css = ".wrike-shareeditor-small")
    private ShareEditorBlock shareEditor;

    public NavigationPanel navigationPanel() {
        return navigationPanel;
    }

    public TaskViewPanel taskViewPanel() {
        return taskViewPanel;
    }

    public TaskListPanel taskListPanel() {
        return taskListPanel;
    }

    public ShareEditorBlock shareEditor() {
        return shareEditor;
    }
}