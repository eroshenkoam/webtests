package com.wrike.webtests.pages;

import com.wrike.webtests.blocks.loginpage.LoginBlock;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class LoginPage {

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(driver)), this);
    }

    @Name("Блок «Логин»")
    @FindBy(css = ".login-block")
    private LoginBlock loginBlock;

    public LoginBlock loginBlock() {
        return loginBlock;
    }

}
