package com.wrike.webtests;

import com.google.inject.Singleton;
import com.google.inject.AbstractModule;
import com.wrike.webtests.webdriver.DefaultWebDriverManager;
import com.wrike.webtests.webdriver.WebDriverManager;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class WebTestsModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(WebDriverManager.class).to(DefaultWebDriverManager.class).in(Singleton.class);
    }

}
