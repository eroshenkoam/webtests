package com.wrike.webtests;

import com.carlosbecker.guice.GuiceModules;
import com.carlosbecker.guice.GuiceTestRunner;
import com.google.inject.Inject;
import com.wrike.webtests.beans.User;
import com.wrike.webtests.pages.ProjectPage;
import com.wrike.webtests.rules.WebDriverRule;
import com.wrike.webtests.steps.UserSteps;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.yandex.qatools.allure.annotations.Title;

import static com.wrike.webtests.TestData.getAvailableUser;
import static com.wrike.webtests.TestData.getRandomProjectName;
import static com.wrike.webtests.TestData.getRandomTaskName;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
@RunWith(GuiceTestRunner.class)
@GuiceModules(WebTestsModule.class)
public class ShareEditorTest {

    @Inject
    public UserSteps userSteps;

    @Rule
    @Inject
    public WebDriverRule webDriverRule;

    @Test
    @Title("Должны проапдейтить меню доступа в другом окне, после добавления новых людей в задачу")
    public void shouldUpdateShareEditorInOtherWindowAfterAddNewTeamToTask() {
        String projectName = getRandomProjectName();
        String taskName = getRandomTaskName();
        User user = getAvailableUser();
        String teamName = "My Team";

        userSteps.loginSteps().login(user.getEmail(), user.getPassword());
        userSteps.projectSteps().createNewProject(projectName).createNewTask(taskName);
        userSteps.defaultSteps().clickOn(onProjectPage().taskViewPanel().shareButton());

        String baseWindow = userSteps.defaultSteps().windowHandle();
        userSteps.defaultSteps().openNewWindowOnSameUrl()
                .clickOn(onProjectPage().taskViewPanel().shareButton())
                .shouldSee(onProjectPage().shareEditor())
                .clicksOnElementWithText(onProjectPage().shareEditor().usersToShareList(), teamName)
                .shouldSee(onProjectPage().shareEditor().notContactsToShare())
                .shouldSeeListCount(onProjectPage().shareEditor().usersAcceptedList(), 2)
                .shouldSeeListCount(onProjectPage().shareEditor().usersToShareList(), 0)
                .closeCurrentWindow()
                .switchTo(baseWindow)
                .shouldSeeListCount(onProjectPage().shareEditor().usersAcceptedList(), 2)
                .shouldSeeListCount(onProjectPage().shareEditor().usersToShareList(), 0);
    }

    private ProjectPage onProjectPage() {
        return new ProjectPage(webDriverRule.getDriver());
    }
}
