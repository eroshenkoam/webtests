package com.wrike.webtests.webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class DefaultWebDriverManager implements WebDriverManager {

    private WebDriver driver;

    public void start() {
        if (driver == null) {
            driver = new FirefoxDriver();
        }
    }

    public void stop() {
        if (driver != null) {
            driver.close();
            driver.quit();
            driver = null;
        }
    }

    public WebDriver get() {
        return driver;
    }
}
