package com.wrike.webtests.webdriver;

import com.google.inject.Provider;
import org.openqa.selenium.WebDriver;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public interface WebDriverManager extends Provider<WebDriver>{

    void start();

    void stop();

    WebDriver get();
}
