package com.wrike.webtests.rules;

import com.google.inject.Inject;
import com.wrike.webtests.webdriver.WebDriverManager;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Attachment;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class WebDriverRule extends TestWatcher {

    private WebDriverManager driverManager;

    @Inject
    public WebDriverRule(WebDriverManager driverManager) {
        this.driverManager = driverManager;
    }

    protected void starting(Description description) {
        driverManager.start();
    }

    @Override
    protected void failed(Throwable e, Description description) {
        makeScreenshotOnFailure();
    }

    protected void finished(Description description) {
        driverManager.stop();
    }

    @Attachment("Screenshot on failure")
    public byte[] makeScreenshotOnFailure() {
        try {
            return ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
        } catch (Exception e) {
            return new byte[]{};
        }
    }

    public WebDriver getDriver() {
        return this.driverManager.get();
    }
}
