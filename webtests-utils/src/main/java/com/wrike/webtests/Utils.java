package com.wrike.webtests;

import org.hamcrest.Matcher;
import ru.yandex.qatools.htmlelements.matchers.decorators.MatcherDecoratorsBuilder;

import static java.util.concurrent.TimeUnit.SECONDS;
import static ru.yandex.qatools.htmlelements.matchers.decorators.MatcherDecoratorsBuilder.should;
import static ru.yandex.qatools.htmlelements.matchers.decorators.TimeoutWaiter.timeoutHasExpired;

/**
 * @author Artem Eroshenko <eroshenkoam@me.com>
 */
public class Utils {

    public static <T> MatcherDecoratorsBuilder<T> withWaitFor(Matcher<? super T> matcher) {
        return should(matcher).whileWaitingUntil(timeoutHasExpired(SECONDS.toMillis(15)));
    }

    public static <T> MatcherDecoratorsBuilder<T> withWaitFor(Matcher<? super T> matcher, long timeout) {
        return should(matcher).whileWaitingUntil(timeoutHasExpired(timeout));
    }

}
